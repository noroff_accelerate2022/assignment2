# Assignment 2 - Data access with JDBC
Contributors: Weronika Zofia Kaluza, Melissa Tuncer

### Working on the project

When adding new functionality, remember to create a new feature branch and then a pull request before merging to master.


### Setting up the SuperheroDb database

In the `database` directory, you will find all the `SQL` scripts required to create the PostgreSQL database, initialize tables and insert tuples.
There are also a couple of other scripts that manipulate the data and add constraints.

Use pgAdmin or `psql` tool in your terminal/command prompt in order to execute the scripts. The database can either be created by
running the `00_createDatabase.sql` script, or by using the pgAdmin tool. 

### Chinook database and spring application

The `assignment2` directory contains the spring application using JDBC and PostgreSQL driver for data access to the Chinook database.

The project has the following structure:

```
| java/com/example/assignment2
    | models
         | records representing a tuple in customer and genre database
         | records representing aggregetions, such as CustomerCountru, CustomerGenre, etc.
    | repository
        | general CRUD repository interface describing general methods for data withdrawal and manipulation
        | CustomerRepository interface describing data access method specific for customer table
        | implementation of interfaces described above
    | AppRunner.java containing tests and Assignment2Application containing the main method
| resources
    | application.properties containing credentials for Chinook database
```

In the `src/main/java/com/example/assignment2` directory, you will find the `AppRunner.java` file, which
contains several test methods for various data access functionalities implemented through the repository pattern.
These methods are being called in `run` method, but are currently commented out
as they require connection to the database which is hosted locally.

### CI pipeline

A gitlab CI pipeline is set up for the project. It only performs a single maven build 
while pushing to the main branch. An artifact with a `.jar` file containing the application is also being generated. In order to avoid pipeline issues, remember to 
comment out the tests before pushing to main, as all methods require
connection to the database, which is not available remotely.

