INSERT INTO power (power_name, power_description) VALUES ('Big Brain Power', 'Extreme intelligence');
INSERT INTO power (power_name, power_description) VALUES ('The Fist', 'Can knock out Godzilla with one hit. Not recommended for certain activities.');
INSERT INTO power (power_name, power_description) VALUES ('Four Eyes', 'Can put glasses on back of head and see every student trying to cheat on a test');
INSERT INTO power (power_name, power_description) VALUES ('Smexyness', 'Combines intelligence with extreme sexyness. Can kill you with one look.');

INSERT INTO superhero_power (superhero_id, power_id) VALUES (01, 01);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (01, 02);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (01, 04);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (02, 02);
INSERT INTO superhero_power (superhero_id, power_id) VALUES (03, 03);