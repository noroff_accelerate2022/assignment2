package com.example.assignment2.repository;

import com.example.assignment2.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImp implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;


    public CustomerRepositoryImp(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = createCustomer(result);
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            //Handle result
            while(result.next()) {
                customer = createCustomer(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer(customer_id, first_name, last_name, country, " +
                "postal_code, phone, email) VALUES (?,?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customerId());
            statement.setString(2,customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phone());
            statement.setString(7, customer.email());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Customer customer) {
        String sql = """
                UPDATE customer
                SET first_name = ?,
                 last_name = ?,
                 country = ?,
                 postal_code = ?,
                 phone = ?,
                 email = ?
                WHERE customer_id = ?;
                """;
        int affectedRows = 0;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2,customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.customerId());
            // Execute statement
            affectedRows = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRows;
    }

    @Override
    public Customer findByName(String firstName) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name LIKE ?" ;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,"%" + firstName + "%");
            // Execute statement
            ResultSet result = statement.executeQuery();
            //Handle result
            while(result.next()) {
                customer = createCustomer(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> returnPageOfCustomers(int limit, int offset) {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,limit);
            statement.setInt(2,offset);
            // Execute statement
            ResultSet result = statement.executeQuery();
            //Handle result
            while(result.next()) {
                Customer customer = createCustomer(result);
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public CustomerCountry getCountryWithMostCustomers() {
        String sql = """
                SELECT country, count(country) AS number_customers
                FROM customer
                GROUP BY country
                ORDER BY number_customers DESC
                LIMIT 1;
                """;
        CustomerCountry customerCountry = null;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute the query
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customerCountry = new CustomerCountry(result.getString("country"), result.getInt("number_customers"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerCountry;
    }

    public CustomerSpender getCustomerThatSpentMost() {
        String sql = """
                SELECT A.*, sum(B.total) AS total_spent
                FROM customer AS A JOIN invoice AS B
                ON A.customer_id = B.customer_id
                GROUP BY A.customer_id
                ORDER BY total_spent DESC
                LIMIT 1;
                """;
        CustomerSpender customerSpender = null;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute the query
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                Customer customer = createCustomer(result);
                customerSpender = new CustomerSpender(customer, result.getInt("total_spent"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    public List<CustomerGenre> getCustomersMostPopularGenre(Customer customer) {
        String sql = """
                SELECT A.customer_id, E.genre_id, E.name, count(*) AS times_bought
                FROM customer AS A JOIN invoice AS B ON (A.customer_id = B.customer_id)\s
                JOIN invoice_line AS C ON (B.invoice_id = C.invoice_id)\s
                JOIN track AS D ON (C.track_id = D.track_id)
                JOIN genre AS E ON (D.genre_id = E.genre_id)
                WHERE A.customer_id = ?
                GROUP BY A.customer_id, E.genre_id
                HAVING count(*) IN (
                	SELECT max(F.times_bought)
                	FROM (
                	SELECT count(*) AS times_bought
                	FROM customer AS A JOIN invoice AS B ON (A.customer_id = B.customer_id)\s
                	JOIN invoice_line AS C ON (B.invoice_id = C.invoice_id)\s
                	JOIN track AS D ON (C.track_id = D.track_id)
                	JOIN genre AS E ON (D.genre_id = E.genre_id)
                	WHERE A.customer_id = ?
                	GROUP BY A.customer_id, E.genre_id
                	) AS F
                );
                """;
        List<CustomerGenre> customerGenres = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Pass the parameters
            statement.setInt(1, customer.customerId());
            statement.setInt(2, customer.customerId());
            // Execute the query
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Genre genre = new Genre(result.getInt("genre_id"), result.getString("name"));
                customerGenres.add(new CustomerGenre(customer, genre));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerGenres;
    }

    /**
     * Creates a new Customer.
     * @param result ResultSet
     * @return Customer
     */
    private Customer createCustomer(ResultSet result) throws SQLException {
        return new Customer(
            result.getInt("customer_id"),
            result.getString("first_name"),
            result.getString("last_name"),
            result.getString("country"),
            result.getString("postal_code"),
            result.getString("phone"),
            result.getString("email"));
    }
}
