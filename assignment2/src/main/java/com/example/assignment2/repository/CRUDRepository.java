package com.example.assignment2.repository;

import java.util.List;


public interface CRUDRepository<T, U> {
    /**
     * Fetches all customers in DB.
     * @return List of Customer
     */
    List<T> findAll();

    /**
     * Fetches customer whose ID matches the input.
     * @param id ID of customer
     * @return Customer
     */
    T findById(U id);

    /**
     * Adds input customer to customer table.
     * @param object Customer
     * @return Number of affected rows.
     */
    int insert(T object);

    /**
     * Updates a whole row in the customer table.
     * <p>
     * All attributes specified in the Customer record should be provided.
     * If some values should remain the same, include old values in the object.
     *<p>
     * The ID of Customer record should be equal to customer's ID that is supposed to be updated.
     *
     * @param object  The Customer record that includes the new values to be updated.
     * @return Number of affected rows
     */
    int update(T object);
}