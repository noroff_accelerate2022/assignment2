package com.example.assignment2.repository;

import com.example.assignment2.models.Customer;
import com.example.assignment2.models.CustomerCountry;
import com.example.assignment2.models.CustomerGenre;
import com.example.assignment2.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer>{
    /**
     * Fetches the customer whose name contains the parameter.
     * @param firstName First name of customer
     * @return Customer
     */
    Customer findByName(String firstName);

    /**
     * Fetches a number of customers based on the input.
     * @param limit Number of customers in DB before those to fetch
     * @param offset Number of customers to fetch
     * @return List of Customer
     */
    List<Customer> returnPageOfCustomers(int limit, int offset);

    /**
     * Returns the country with the most customers in DB
     * @return CustomerCountry
     */
    CustomerCountry getCountryWithMostCustomers();

    /**
     * Fetches customer in DB who spent the most money.
     * @return CustomerSpender
     */
    CustomerSpender getCustomerThatSpentMost();

    /**
     * Returns the favorite music genre of the input customer.
     * @param customer Customer
     * @return CustomerSpender
     */
    List<CustomerGenre> getCustomersMostPopularGenre(Customer customer);
}