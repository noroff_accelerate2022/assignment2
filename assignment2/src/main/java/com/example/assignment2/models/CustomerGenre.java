package com.example.assignment2.models;

public record CustomerGenre(Customer customer, Genre genre){
}
