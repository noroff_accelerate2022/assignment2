package com.example.assignment2.models;

public record Customer(
        int customerId,
        String firstName,
        String lastName,
        String country,
        String postalCode,
        String phone,
        String email) {
}