package com.example.assignment2.models;

public record Genre (
        int genre_id,
        String name){
}
