package com.example.assignment2.models;

public record CustomerSpender(
        Customer customer,
        int totalSpent
){
}
