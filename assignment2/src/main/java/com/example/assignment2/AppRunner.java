package com.example.assignment2;

import com.example.assignment2.models.Customer;
import com.example.assignment2.models.CustomerCountry;
import com.example.assignment2.models.CustomerGenre;
import com.example.assignment2.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import com.example.assignment2.repository.CustomerRepository;

import java.util.List;

@Component
public class AppRunner implements ApplicationRunner {
   private final CustomerRepository customerRepository;

    public AppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void findAll(){
        System.out.println(customerRepository.findAll());
    }

    public void findById() {
        System.out.println(customerRepository.findById(2));
    }

    public void insert() {
        Customer nicholas = new Customer(10002, "Nicholas", "Lennox",
                "Norway", "0666", "12345678", "lenny@hotmail.com");
        System.out.println(customerRepository.insert(nicholas));
    }

    public void findByName() {
        System.out.println(customerRepository.findByName("Hel"));
    }

    public void returnPage() {
        System.out.println(customerRepository.returnPageOfCustomers(2,3));
    }

    public void getCountryWithMostCustomers() {
        CustomerCountry cc = customerRepository.getCountryWithMostCustomers();
        System.out.println("Country with most customers: " + cc.country() + ". Number customers: " + cc.numberCustomers());
    }

    public void getCustomerThatSpentMost() {
        CustomerSpender c = customerRepository.getCustomerThatSpentMost();
        System.out.println("Customer that spent most money: " + c.customer().firstName() + " " + c.customer().lastName());
        System.out.println("Total amount: " + c.totalSpent());
    }

    public void getCustomersMostPopularGenre() {
        Customer customer = customerRepository.findById(1);
        List<CustomerGenre> cg = customerRepository.getCustomersMostPopularGenre(customer);
        System.out.println("Customer " + customer.firstName() + " " + customer.lastName());
        cg.forEach(customerGenre -> System.out.println(customerGenre.genre().name()));
    }

    public void update() {
        Customer c = new Customer(60, "Alfred", "Blue", "Sri Lanka", "1234", "23454321", "alfred@a.com");
        System.out.println(customerRepository.update(c));
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //Big Brain's tests
        //findAll();
        //findById();
        //insert();
        //findByName();
        //returnPage();




        //System.out.println(customerRepository.update());
        //System.out.println(customerRepository.delete());
        //System.out.println(customerRepository.deleteById());

        //golonka's tests
        //getCountryWithMostCustomers();
        //getCustomerThatSpentMost();
        //getCustomersMostPopularGenre();
        //update();

    }
}
